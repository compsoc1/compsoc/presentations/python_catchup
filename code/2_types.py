"""
In this example, we learn about the basic types available in python
"""

# An integer is any whole number
integer_var = 23

# A float is any mathematical rational value
# But also has the ability to represent special values
# such as NaN, Inf, and -Inf
float_var = 12.47

string_var = "Skynet is Alive"

# A Boolean is either True or False
# and you can logical ANDs, ORs, NOTs, etc. on them
boolean_var = True


# A list is, as the name suggessts, a list of data 
# in python, the elements of a list can have different types 
# as demonstrated below
list_var = [42, 3.14159, "Heehee", False]


# A Dictionary is a mapping of keys into values
# These keys can be integers or strings
# And the values can be just about anything
dict_var = {
    "Jan" : "January",
    "Feb" : "February",
    "Mar" : "March",
    "Apr" : "April",
    "May" : "May",
    "Jun" : "June",
    "Jul" : "July",
    "Aug" : "August",
    "Sep" : "September",
    "Oct" : "October",
    "Nov" : "November",
    "Dec" : "December"
}
