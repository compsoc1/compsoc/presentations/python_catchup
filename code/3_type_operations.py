"""
In this example, we will cover the operations that one can do on each of the primitive types

"""


"""
On Integers, we can do all arithmetic operations
The result type of a binary operation on two integers is an integer 

One consequence of this, is that the division operator will implicitly floor the result because integers cannot represent decimal numbers

On a division by 0, python will create a ZeroDivisionError, we will cover those later
"""

number1 = 11
number2 = 12

sum_var = number1 + number2 # 13
dif_var = number1 - number2 # -1
product_var = number1 * number2 # 132
quotient_var = number1 / number2 # 0 (Floor of 11/12)


"""
On Floats, we have the exact same operations available as we do on integers
The result type of a binary operation on two floats is a float 

Because of this, division has the proper expected behavior with floats

But prepare for a surprise!!
Try printing out the sum_var after it is assigned, what value do you get?

It should be 0.3, right?
But you are getting 0.30000...0004,How Strange!
This is actually a result of how computers store floating-point numbers 

You know the way some numbers for us, such as 1/3 have a reccuring decimal place?
In different numbering systems (bases), the numbers which require an infinite number of decimal places to represent changes
In base 2 (binary), 3/10 needs infinite "decimals" to represent, since you cannot write 3/10 as the finite summation of 2^-x 
It would require infinite decimal places, i.e infinite memory to represent, which is not possible
so all we can do is approximate for it!
Look up IEE 754 for more info https://en.wikipedia.org/wiki/IEEE_754

"""

float1 = 0.1
float2 = 0.2

sum_var = float1 + float2        # 0.3
dif_var = float1 - float2        # -0.1
product_var = float1 * float2    # 0.02
quotient_var = float1 / float2   # 0.5

"""
With Booleans, we can use all the logic operators that you covered in Discrete Math
We can do boolean operators using their names, or their symbols
and &&
or  ||
not !
xor ^   The xor keyword does not work

"""

bool1 = True
bool2 = True

and_var = bool1 and bool2 # False
or_var  = bool1 or bool2  # True
not_var = not bool1       # False
xor_var = bool1 ^ bool2   # True


"""
Strings support basic operations, like repetition, and joining them together
You can think of strings as simply being a list of characters. Because of this, most list operations
also apply to strings. We will cover those in a minute

But they also have more things you can do to them by using 'methods' on them


Some of the demonstrated methods include:
.split(at) => Splits the string into a list of substrings at the provided split point
.capitalize() => Capitalizes the string
.startswith(str), .endswith(str) => Check if the string starts with another string and ends with another string

Your editor should tell you all the methods available to you if you type out the string variable name and a dot
"""


string1 = "Skynet is "
string2 = "Catlover"
string3 = "Coming"

sum_var = string1 + string3 # "Skynet is Coming"
product_var = string2 * 3 # CatloverCatloverCatlover

words = sum_var.split(" ") # Splits into words ["Skynet", "is", "Coming"]
capital = sum_var.capitalize() # Capitalizes "SKYNET IS COMING"
startswith = sum_var.startswith("Skynet") # True 
endswith = sum_var.endswith("Peanuts") # False



"""
Lists are one of the most useful data types in python, specifically when it comes to algorithms of any kind 

You are able to:
get the length of the list
get elements at specific positions of the list 
get subsets of the list using slicing

A quirk of lists in most programming languages is that they start at the 0th element 
this is because the index is historically an offset from the first element in the list 
But algorithmically, a zero-indexing system is surprisingly nicer to work with

In python, slicing is not inclusive of the final element
If you omit either end of the slicing operation, it implicitly takes the rest of the array

You can also do things like appending and repeating arrays
"""

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


length = len(list1) # Gets the length of the list => 10 

first = list1[0] # Gets the first element of the list   => 1
second = list1[1] # Gets the second element of the list => 2
last = list1[-1] # Gets the last element of the list    => 10

first_five = list1[0:5]
# is also 
first_five = list1[:5]


last_five = list1[5:10]
# is also 
last_five = list1[5:]

list2 = last_five + first_five # We have swapped the first and last 5 elements around
rep = first_five * 3 # [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5]

list2.append(27) # We can add a new element to the list

list2.sort() # The elements of list2 get sorted (in-place)


list2.reverse() # We reverse the sorted list 

one = list2.pop() # Remove the last element from the list (list2[-1]) which is 1

print(list2)

"""
Dictionaries are the final builtin type we are covering
They allow us to map elements to other elements, and subsequently look them up 
They do this quite efficiently
"""

months_abbreviations = {
    "Jan" : "January",
    "Feb" : "February",
    "Mar" : "March",
    "Apr" : "April",
    "May" : "May",
    "Jun" : "June",
    "Jul" : "July",
    "Aug" : "August",
    "Sep" : "September",
    "Oct" : "October",
    "Nov" : "November",
    "Dec" : "December"
}

months_abbreviations["Jan"] # "January"

del months_abbreviations["Sep"] # We just delete september, who liked it anyway?

months_abbreviations["Ala"] = "Alabama" # Who said states cant be months?




"""
Dictionaries are a very robust type, and can also be used for things like tracking users and their scores
"""


users = {
        "mario"   : 11,
        "luigi"   : 13,
        "peach"   : 17,
        "starfox" : 174
}


for num in range(10):
    print(num)
