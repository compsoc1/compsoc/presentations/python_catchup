"""
In this example, we will learn how to use variables in python

FYI: The thing in which I am currently writing is called a comment,
this lets us explain our code.
Comments can take this form, or the hashtag form

Terms:

RHS : Right Hand Side 
LHS : Left Hand Side
"""

# Creating a new variable
foo = 30

bar = 12


# We can see the values inside of variables by printing them to the terminal
# Python has a special builtin function called 'print' that does this for us 
print(foo)

print(bar)

# We assign the result of the expression on the RHS to the name on the LHS
# In this case, the expression on the right is the sum of the variables 'foo'
# and 'bar'
summed = foo + bar


# Printing the sum of foo and bar 
# This should output '42' to the screen
print(summed)


# You can also re-assign variables
bar = 39

# This re-assignment does not affect the value of 'summed'
print(summed) # Still 42

# This is because python variables are immediately evaluated on assignment
